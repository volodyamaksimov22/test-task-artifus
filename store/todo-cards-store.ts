import { defineStore } from 'pinia';
import {type Card, type CardsStore, Status} from '~/types';

export const useCardsStore = defineStore('cards-store', {
  state: (): CardsStore => {
    return {
      cards: [],
    };
  },

  actions: {
    createTask(value: string) {
      this.$state.cards.push({
        value: value,
        status: Status.created,
      })
    },
    deleteTask(card: Card) {
      this.$state.cards = this.$state.cards.filter(c => c !== card)
    },
    checkTask(card: Card) {
      const checkingCard = this.$state.cards.find(c => c === card)
      // @ts-ignore
      checkingCard.status = Status.done
    }
  },
});
