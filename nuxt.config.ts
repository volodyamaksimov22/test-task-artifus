// https://nuxt.com/docs/api/configuration/nuxt-config
import svgLoader from 'vite-svg-loader';

export default defineNuxtConfig({
  devtools: { enabled: false },
  css: ['/assets/style/main.sass'],
  modules: ['@pinia/nuxt'],
  vite: {
    plugins: [svgLoader()],
  },
});
