export enum Status {
  done = 'done',
  created = 'created',
}

export interface Card {
  value: string
  status: Status.done | Status.created
}

export interface CardsStore {
  cards: Card[]


}
